<?php

namespace App\Http\Controllers\APUPPT;

use App\Rules;
use App\Models;
use App\Helpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\DataGridRequest;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades;
use Vinkla\Hashids\Facades\Hashids;

use App\Imports\ProvinceManagementImport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProvinceManagementController extends Controller
{
  const MODULE_PATH = "apu-ppt/province-management";

  /**
   * Get the list of all postal code
   *
   * @param  \App\Http\Requests\DataGridRequest  $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function getList(DataGridRequest $request): JsonResponse
  {
    $result = Models\ProvincePostalCode::whereNull("timestampDeleted")->get();

    $result = $result->encode(["provincePostalCodeID"]);

    if ($request->filled("search")) {
      $search = $request->search;

      $result = $result->filter(function ($row) use ($search) {
        return collect($row)->contains(function ($value) use ($search) {
          return stripos($value, $search) !== false;
        });
      });
    }

    $sortField = $request->sortField ?? "min";

    $sortOrder =
      !$request->filled("sortOrder") || $request->sortOrder === "ASC"
        ? "sortBy"
        : "sortByDesc";

    $result = $result->$sortOrder($sortField, SORT_FLAG_CASE | SORT_NATURAL);

    $total = $result->count();

    $rows = $result
      ->forPage($request->paginationPage, $request->paginationRows)
      ->values();

    return response()->json(["rows" => $rows, "total" => $total]);
  }

  /**
   * Get postal code record
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function getItem(Request $request): JsonResponse
  {
    $request->validate([
      "id" => ["required", "alpha_num", new Rules\Hashids()],
    ]);

    $provincePostalCodeID = Hashids::decode($request->id)[0];

    Facades\Validator::make(
      [
        "provincePostalCodeID" => $provincePostalCodeID,
      ],
      [
        "provincePostalCodeID" => [
          "exists:province_postal_code,provincePostalCodeID",
        ],
      ],
    )->validate();

    $row = Models\ProvincePostalCode::find($provincePostalCodeID);

    $row = collect($row)->encode(["provincePostalCodeID"]);

    if ($row->isEmpty()) {
      throw (new ModelNotFoundException())->setModel("Province Postal Code");
    }

    return response()->json($row);
  }

  /**
   * Add new postal code record
   *
   * @param Request $request
   * @return mixed
   */
  public function create(Request $request)
  {
    $param = [self::MODULE_PATH, Facades\Config::get("constants.feature.add")];

    if (!Facades\Gate::authorize("feature-access", $param)) {
      return;
    }

    $request->validate(
      [
        "min" => ["required", "numeric", "min:1"],
        "max" => ["required", "numeric", "gt:min"],
        "province" => [
          "required",
          function ($attr, $value, $fail) {
            if (preg_match("/^[A-Z]{1,}([A-Z\s,.'])*$/", $value) == 0) {
              $fail(
                "The Province field may only contain capital letters, spaces, and special characters . , '",
              );
            }
          },
        ],
        "provinceCode" => ["nullable", "digits:2"],
      ],
      [
        "min.required" => "The Postal Code Min field is required.",
        "min.numeric" => "The Postal Code Min field must be a number.",
        "min.min" => "The Postal Code Min field must be at least 1.",
        "max.required" => "The Postal Code Max field is required.",
        "max.numeric" => "The Postal Code Max field must be a number.",
        "max.gt" =>
          "The Postal Code Max field must be greater than Postal Code Min.",
      ],
    );

    $postData = collect($request->input())->toArray();

    $provinceCodeRules =
      isset($postData["provinceCode"]) && !is_null($postData["provinceCode"])
        ? [
          "provinceCode" => function ($attribute, $value, $fail) {
            $this->validateUniqueFromExistingRows($value, $fail);
          },
        ]
        : [];

    Facades\Validator::make(
      $postData,
      array_merge(
        [
          "min" => function ($attribute, $value, $fail) use ($postData) {
            $this->validateRangeFromExistingRows(
              "Postal Code Min",
              $postData["min"],
              $postData["max"],
              $fail,
            );
          },
          "max" => function ($attribute, $value, $fail) use ($postData) {
            $this->validateRangeFromExistingRows(
              "Postal Code Max",
              $postData["min"],
              $postData["max"],
              $fail,
            );
          },
        ],
        $provinceCodeRules,
      ),
    )->validate();

    $config = [
      "event" => "Create",
      "userAgent" => $request->server('HTTP_USER_AGENT'),
      "ipAddress" => $request->ip(),
      "timestamp" => timestamp(),
    ];

    Facades\DB::transaction(function () use ($postData, $config) {
      $postalCode = new Models\ProvincePostalCode();

      $postalCode->province = $postData["province"];

      $postalCode->provinceCode = $postData["provinceCode"] ?? null;

      $postalCode->min = $postData["min"];

      $postalCode->max = $postData["max"];

      $postalCode->save();

      # Audit Log

      $config["referenceID"] = $postalCode->provincePostalCodeID;

      Helpers\AuditLog::create("province-postal-code", $config, [], $postData);
    });

    return response()->json([
      "message" => "New postal code has been successfully created!",
    ]);
  }

  /**
   * Update postal code detail
   *
   * @param Request $request
   * @return mixed
   */
  public function update(Request $request)
  {
    $param = [self::MODULE_PATH, Facades\Config::get("constants.feature.edit")];

    if (!Facades\Gate::authorize("feature-access", $param)) {
      return;
    }

    $request->validate(
      [
        "provincePostalCodeID" => [
          "required",
          "alpha_num",
          new Rules\Hashids(),
        ],
        "min" => ["required", "numeric", "min:1"],
        "max" => ["required", "numeric", "gt:min"],
        "province" => [
          "required",
          function ($attr, $value, $fail) {
            if (preg_match("/^[A-Z]{1,}([A-Z\s,.'])*$/", $value) == 0) {
              $fail(
                "The Province field may only contain capital letters, spaces, and special characters . , '",
              );
            }
          },
        ],
        "provinceCode" => ["nullable", "digits:2"],
      ],
      [
        "min.required" => "The Postal Code Min field is required.",
        "min.numeric" => "The Postal Code Min field must be a number.",
        "min.min" => "The Postal Code Min field must be at least 1.",
        "max.required" => "The Postal Code Max field is required.",
        "max.numeric" => "The Postal Code Max field must be a number.",
        "max.gt" =>
          "The Postal Code Max field must be greater than Postal Code Min.",
      ],
    );

    $postData = collect($request->input())
      ->decode(["provincePostalCodeID"])
      ->toArray();

    $provincePostalCodeID = $postData["provincePostalCodeID"];

    $provinceCodeRules =
      isset($postData["provinceCode"]) && !is_null($postData["provinceCode"])
        ? [
          "provinceCode" => function ($attribute, $value, $fail) use (
            $provincePostalCodeID
          ) {
            $this->validateUniqueFromExistingRows(
              $value,
              $fail,
              $provincePostalCodeID,
            );
          },
        ]
        : [];

    Facades\Validator::make(
      $postData,
      array_merge(
        [
          "provincePostalCodeID" => [
            "exists:province_postal_code,provincePostalCodeID",
          ],
          "min" => function ($attribute, $value, $fail) use (
            $provincePostalCodeID,
            $postData
          ) {
            $this->validateRangeFromExistingRows(
              "Postal Code Min",
              $postData["min"],
              $postData["max"],
              $fail,
              $provincePostalCodeID,
            );
          },
          "max" => function ($attribute, $value, $fail) use (
            $provincePostalCodeID,
            $postData
          ) {
            $this->validateRangeFromExistingRows(
              "Postal Code Max",
              $postData["min"],
              $postData["max"],
              $fail,
              $provincePostalCodeID,
            );
          },
        ],
        $provinceCodeRules,
      ),
    )->validate();

    $oldData = Models\ProvincePostalCode::find(
      $postData["provincePostalCodeID"],
    )->toArray();

    $config = [
      "event" => "Update",
      "referenceID" => $postData["provincePostalCodeID"],
      "userAgent" => $request->server('HTTP_USER_AGENT'),
      "ipAddress" => $request->ip(),
      "timestamp" => timestamp(),
    ];

    Facades\DB::transaction(function () use ($oldData, $postData, $config) {
      # Update ProvincePostalCode

      $postalCode = Models\ProvincePostalCode::find(
        $postData["provincePostalCodeID"],
      );

      $postalCode->province = $postData["province"];

      $postalCode->provinceCode = $postData["provinceCode"] ?? null;

      $postalCode->min = $postData["min"];

      $postalCode->max = $postData["max"];

      $postalCode->save();

      # Audit Log

      Helpers\AuditLog::create(
        "province-postal-code",
        $config,
        $oldData,
        $postData,
      );
    });

    return response()->json([
      "message" => "Postal code record has been successfully updated!",
    ]);
  }

  /**
   * Delete postal code record
   *
   * @param Request $request
   * @return mixed
   */
  public function delete(Request $request)
  {
    $param = [
      self::MODULE_PATH,
      Facades\Config::get("constants.feature.delete"),
    ];

    if (!Facades\Gate::authorize("feature-access", $param)) {
      return;
    }

    $request->validate([
      "id" => ["sometimes", "nullable", "alpha_num", new Rules\Hashids()],
    ]);

    $provincePostalCodeID = null;

    if ($request->filled("id")) {
      $provincePostalCodeID = Hashids::decode($request->id)[0];

      Facades\Validator::make(
        [
          "provincePostalCodeID" => $provincePostalCodeID,
        ],
        [
          "provincePostalCodeID" => [
            "exists:province_postal_code,provincePostalCodeID",
          ],
        ],
      )->validate();
    }

    if (is_null($provincePostalCodeID)) {
      $deleteRecords = Models\ProvincePostalCode::whereNull(
        "timestampDeleted",
      )->get();

      $provincePostalCodeIDs = $deleteRecords->pluck("provincePostalCodeID");
    } else {
      $deleteRecords = Models\ProvincePostalCode::where(
        "provincePostalCodeID",
        $provincePostalCodeID,
      )->get();

      $provincePostalCodeIDs = [$provincePostalCodeID];
    }

    $oldRecords = $deleteRecords->toArray();

    $config = [
      "event" => "Delete",
      "userAgent" => $request->server('HTTP_USER_AGENT'),
      "ipAddress" => $request->ip(),
      "timestamp" => timestamp(),
    ];

    Facades\DB::transaction(function () use (
      $provincePostalCodeIDs,
      $oldRecords,
      $config
    ) {
      Models\ProvincePostalCode::whereIn(
        "provincePostalCodeID",
        $provincePostalCodeIDs,
      )->update(["timestampDeleted" => $config["timestamp"]]);

      # Audit Log

      foreach ($oldRecords as $oldRecord) {
        $config["referenceID"] = $oldRecord["provincePostalCodeID"];
        
        Helpers\AuditLog::create(
          "province-postal-code",
          $config,
          $oldRecord,
          [],
        );
      }
    });

    return response()->json([
      "message" => "Postal code record(s) has been successfully deleted!",
    ]);
  }

  /**
   * Create new postal codes from excel file
   *
   * @param Request $request
   * @return void
   */
  public function upload(Request $request)
  {
    $param = [self::MODULE_PATH, Facades\Config::get("constants.feature.add")];

    if (!Facades\Gate::authorize("feature-access", $param)) {
      return;
    }

    $request->validate([
      "provincePostalCodeFile" => ["required", "mimes:xls,xlsx"],
    ]);

    $path = $request->file("provincePostalCodeFile")->getRealPath();

    $extension = $request
      ->file("provincePostalCodeFile")
      ->getClientOriginalExtension();

    $readerType =
      $extension === "xlsx"
        ? \Maatwebsite\Excel\Excel::XLSX
        : \Maatwebsite\Excel\Excel::XLS;

    $import = new ProvinceManagementImport();

    Excel::import($import, $path, null, $readerType);

    $rows = $import->getRows();

    $validator = Facades\Validator::make(
      $rows,
      [
        "*.min" => [
          "required",
          "numeric",
          "min:1",
          function ($attribute, $value, $fail) use ($rows) {
            $attrPart = explode(".", $attribute);

            $this->validateRangeFromFileRows(
              "Postal Code Min",
              $attrPart[0],
              $rows,
              $fail,
            );
          },
          function ($attribute, $value, $fail) use ($rows) {
            $attrPart = explode(".", $attribute);

            $min = $rows[$attrPart[0]]["min"];
            $max = $rows[$attrPart[0]]["max"];

            $this->validateRangeFromExistingRows(
              "Postal Code Min",
              $min,
              $max,
              $fail,
            );
          },
        ],
        "*.max" => [
          "required",
          "numeric",
          "gt:*.min",
          function ($attribute, $value, $fail) use ($rows) {
            $attrPart = explode(".", $attribute);

            $this->validateRangeFromFileRows(
              "Postal Code Max",
              $attrPart[0],
              $rows,
              $fail,
            );
          },
          function ($attribute, $value, $fail) use ($rows) {
            $attrPart = explode(".", $attribute);

            $min = $rows[$attrPart[0]]["min"];
            $max = $rows[$attrPart[0]]["max"];

            $this->validateRangeFromExistingRows(
              "Postal Code Max",
              $min,
              $max,
              $fail,
            );
          },
        ],
        "*.province" => [
          "required",
          function ($attr, $value, $fail) {
            if (preg_match("/^[A-Z]{1,}([A-Z\s,.'])*$/", $value) == 0) {
              $fail(
                "The Province field may only contain capital letters, spaces, and special characters . , '",
              );
            }
          },
        ],
        "*.provinceCode" => [
          "nullable",
          "digits:2",
          function ($attribute, $value, $fail) use ($rows) {
            $attrPart = explode(".", $attribute);
            $index = $attrPart[0];

            if (!is_null($rows[$index]["provinceCode"])) {
              $this->validateUniqueFromExistingRows(
                $rows[$index]["provinceCode"],
                $fail,
              );
            }
          },
          function ($attribute, $value, $fail) use ($rows) {
            $attrPart = explode(".", $attribute);

            $this->validateUniqueFromFileRows($attrPart[0], $rows, $fail);
          },
        ],
      ],
      [
        "*.min.required" => "The Postal Code Min field is required.",
        "*.min.numeric" => "The Postal Code Min field must be a number.",
        "*.min.min" => "The Postal Code Min field must be at least 1",
        "*.max.required" => "The Postal Code Max field is required.",
        "*.max.numeric" => "The Postal Code Max field must be a number.",
        "*.max.gt" =>
          "The Postal Code Max field must be greater than Postal Code Min.",
        "*.province.required" => "The Province field is required.",
        "*.provinceCode.digits" =>
          "The Province Code field must be exactly 2 digits.",
      ],
    );

    $invalid = [];
    $valid = $rows;

    if ($validator->fails()) {
      $errors = $validator->errors()->toArray();

      foreach ($errors as $key => $error) {
        $keyExplode = explode(".", $key);

        $index = $keyExplode[0];

        if (in_array($index, $invalid)) {
          continue;
        }

        $invalid[] = $keyExplode[0]; // push index
      }
    }

    $valid = array_filter(
      $rows,
      function ($index) use ($invalid) {
        return !in_array($index, $invalid);
      },
      ARRAY_FILTER_USE_KEY,
    );

    if (empty($valid)) {
      return response()->json([
        "success" => false,
        "message" => $this->generateErrorMessage($errors),
      ]);
    }

    $validRecords = array_unique($valid, SORT_REGULAR);

    $config = [
      "event" => "Create",
      "userAgent" => $request->server('HTTP_USER_AGENT'),
      "ipAddress" => $request->ip(),
      "timestamp" => timestamp(),
    ];

    Facades\DB::transaction(function () use ($validRecords, $config) {
      foreach ($validRecords as $validRecord) {
        $postalCode = new Models\ProvincePostalCode();

        $postalCode->province = $validRecord["province"];

        $postalCode->provinceCode = $validRecord["provinceCode"] ?? null;

        $postalCode->min = $validRecord["min"];

        $postalCode->max = $validRecord["max"];

        $postalCode->save();

        # Audit Log

        $config["referenceID"] = $postalCode->provincePostalCodeID;

        Helpers\AuditLog::create(
          "province-postal-code",
          $config,
          [],
          $validRecord,
        );
      }
    });

    return empty($invalid)
      ? response()->json([
        "success" => true,
        "message" => "New postal code(s) successfully added.",
      ])
      : response()->json([
        "success" => false,
        "message" => $this->generateErrorMessage($errors, $validRecords),
      ]);
  }

  /**
   * Generate file upload error message
   *
   * @param  array  $errors
   * @param  array  $validRows
   * @return array
   */
  private function generateErrorMessage($errors, $validRows = []): array
  {
    $invalid = [];
    $invalidRows = [];

    if (!empty($validRows)) {
      foreach ($validRows as $key => $validRow) {
        $rowNo = $key + 1;

        $valid[] = "Row {$rowNo}";
      }
    }

    foreach ($errors as $key => $error) {
      $keyExplode = explode(".", $key);

      $index = $keyExplode[0];

      $rowNo = (int) $index + 1;

      $invalidRows["Row {$rowNo}"][] = current($error);
    }

    ksort($invalidRows);

    foreach ($invalidRows as $key => $row) {
      $message = preg_replace("/[0-9]+\.province/", "Province", $row);

      $invalid[] = ["label" => $key, "errors" => $message];
    }

    return empty($valid)
      ? ["invalid" => $invalid]
      : ["valid" => $valid, "invalid" => $invalid];
  }

  /**
   * Validate postal code range from database
   *
   * @param string $attr
   * @param integer $min
   * @param integer $max
   * @param [type] $fail
   * @param integer|null $provincePostalCodeID
   * @return void
   */
  public function validateRangeFromExistingRows(
    string $attr,
    $min,
    $max,
    $fail,
    ?int $provincePostalCodeID = null
  ) {
    $postalCodes = Models\ProvincePostalCode::whereNull(
      "timestampDeleted",
    )->get();

    if (!is_null($provincePostalCodeID)) {
      $postalCodes = $postalCodes->except($provincePostalCodeID);
    }

    $msg = "The {$attr} field must have no overlap range with other postal codes.";

    foreach ($postalCodes as $postalCode) {
      if ($postalCode->min <= $min && $postalCode->max >= $min) {
        $fail($msg);
        continue;
      }

      if ($postalCode->min <= $max && $postalCode->max >= $max) {
        $fail($msg);
        continue;
      }

      if ($min <= $postalCode->min && $max >= $postalCode->min) {
        $fail($msg);
        continue;
      }

      if ($min <= $postalCode->max && $max >= $postalCode->max) {
        $fail($msg);
        continue;
      }
    }
  }

  /**
   * Validate postal code range from rows of excel files
   *
   * @param string $attr
   * @param $index
   * @param array $rows
   * @param [type] $fail
   * @return void
   */
  public function validateRangeFromFileRows(
    string $attr,
    $index,
    array $rows,
    $fail
  ) {
    $msg = "The {$attr} field must have no overlap range with other postal codes.";

    $min = $rows[$index]["min"];
    $max = $rows[$index]["max"];

    foreach ($rows as $idx => $row) {
      if ($idx == $index) {
        continue;
      }

      $fileRowMin = $row["min"];
      $fileRowMax = $row["max"];

      if ($fileRowMin <= $min && $fileRowMax >= $min) {
        $fail($msg);
        continue;
      }

      if ($fileRowMin <= $max && $fileRowMax >= $max) {
        $fail($msg);
        continue;
      }

      if ($min <= $fileRowMin && $max >= $fileRowMin) {
        $fail($msg);
        continue;
      }

      if ($min <= $fileRowMax && $max >= $fileRowMax) {
        $fail($msg);
        continue;
      }
    }
  }

  /**
   * Validate province code unique from database
   *
   * @param $provinceCode
   * @param [type] $fail
   * @param integer|null $provincePostalCodeID
   * @return void
   */
  public function validateUniqueFromExistingRows(
    $provinceCode,
    $fail,
    ?int $provincePostalCodeID = null
  ) {
    $rows = Models\ProvincePostalCode::whereNull("timestampDeleted")->get([
      "provincePostalCodeID",
      "provinceCode",
    ]);

    if (!is_null($provincePostalCodeID)) {
      $rows = $rows->except($provincePostalCodeID);
    }

    foreach ($rows as $row) {
      if ($row->provinceCode == $provinceCode) {
        $fail("The Province Code field has already been taken.");
      }
    }
  }

  /**
   * Validate province code unique from rows of excel files
   *
   * @param $index
   * @param array $rows
   * @param [type] $fail
   * @return void
   */
  public function validateUniqueFromFileRows($index, array $rows, $fail)
  {
    $provinceCode = $rows[$index]["provinceCode"] ?? null;

    if (is_null($provinceCode)) {
      return;
    }

    foreach ($rows as $idx => $row) {
      if ($idx == $index) {
        continue;
      }

      $fileRowProvinceCode = $row["provinceCode"] ?? null;

      if (is_null($fileRowProvinceCode)) {
        continue;
      }

      if ($provinceCode == $fileRowProvinceCode) {
        $fail("The Province Code field has already been taken.");
      }
    }
  }
}
