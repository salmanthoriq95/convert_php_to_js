class ProvinceManagementController extends Controller {
    getList(request, res, next) {
        var result = Models.ProvincePostalCode.whereNull("timestampDeleted").get();
        result = result.encode(["provincePostalCodeID"]);

        if (request.filled("search")) {
            var search = request.search;
            result = result.filter(row => {
                return collect(row).contains(value => {
                    return stripos(value, search) !== false;
                });
            });
        }

        var sortField = request.sortField ?? "min";
        var sortOrder = !request.filled("sortOrder") || request.sortOrder === "ASC" ? "sortBy" : "sortByDesc";
        result = result[sortOrder](sortField, SORT_FLAG_CASE | SORT_NATURAL);
        var total = result.count();
        var rows = result.forPage(request.paginationPage, request.paginationRows).values();
        return response.json({
            rows: rows,
            total: total
        });
    }

};
