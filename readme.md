# CONVERT PHP TO JSON

Using babel, specially babel-cli dependencies and using babel-preset-php as preset for babel. We can convert php classes to javascript classes.

## how to use
```bash
npm i # install babel-cli and babel-preset-php from npm

babel file.php -o file.js # convert php to js
```
Thanks to :
[babel-preset-php ^v1.0.0](https://gitlab.com/kornelski/babel-preset-php)
[babel-cli ^v6.0.0](https://github.com/babel/babel)
