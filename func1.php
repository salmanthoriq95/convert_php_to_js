<?php


class ProvinceManagementController extends Controller
{
  const MODULE_PATH = "apu-ppt/province-management";

 
  public function getList(DataGridRequest $request): JsonResponse
  {
    $result = Models\ProvincePostalCode::whereNull("timestampDeleted")->get();

    $result = $result->encode(["provincePostalCodeID"]);

    if ($request->filled("search")) {
      $search = $request->search;

      $result = $result->filter(function ($row) use ($search) {
        return collect($row)->contains(function ($value) use ($search) {
          return stripos($value, $search) !== false;
        });
      });
    }

    $sortField = $request->sortField ?? "min";

    $sortOrder =
      !$request->filled("sortOrder") || $request->sortOrder === "ASC"
        ? "sortBy"
        : "sortByDesc";

    $result = $result->$sortOrder($sortField, SORT_FLAG_CASE | SORT_NATURAL);

    $total = $result->count();

    $rows = $result
      ->forPage($request->paginationPage, $request->paginationRows)
      ->values();

    return response()->json(["rows" => $rows, "total" => $total]);
  }

 
  public function getItem(Request $request): JsonResponse
  {
    

    $provincePostalCodeID = Hashids::decode($request->id)[0];

    

    $row = Models\ProvincePostalCode::find($provincePostalCodeID);

    $row = collect($row)->encode(["provincePostalCodeID"]);

    if ($row->isEmpty()) {
      throw (new ModelNotFoundException())->setModel("Province Postal Code");
    }

    return response()->json($row);
  }


  public function create(Request $request)
  {
    $param = [self::MODULE_PATH, Facades\Config::get("constants.feature.add")];

    if (!Facades\Gate::authorize("feature-access", $param)) {
      return;
    }

    

    $postData = collect($request->input())->toArray();

    $provinceCodeRules =
      isset($postData["provinceCode"]) && !is_null($postData["provinceCode"])
        ? [
          "provinceCode" => function ($attribute, $value, $fail) {
            $this->validateUniqueFromExistingRows($value, $fail);
          },
        ]
        : [];

  
    $config = [
      "event" => "Create",
      "userAgent" => $request->server('HTTP_USER_AGENT'),
      "ipAddress" => $request->ip(),
      "timestamp" => timestamp(),
    ];

    Facades\DB::transaction(function () use ($postData, $config) {
      $postalCode = new Models\ProvincePostalCode();

      $postalCode->province = $postData["province"];

      $postalCode->provinceCode = $postData["provinceCode"] ?? null;

      $postalCode->min = $postData["min"];

      $postalCode->max = $postData["max"];

      $postalCode->save();

    

      $config["referenceID"] = $postalCode->provincePostalCodeID;

      Helpers\AuditLog::create("province-postal-code", $config, [], $postData);
    });

    return response()->json([
      "message" => "New postal code has been successfully created!",
    ]);
  }

 
  public function update(Request $request)
  {
    $param = [self::MODULE_PATH, Facades\Config::get("constants.feature.edit")];

    if (!Facades\Gate::authorize("feature-access", $param)) {
      return;
    }

  

    $postData = collect($request->input())
      ->decode(["provincePostalCodeID"])
      ->toArray();

    $provincePostalCodeID = $postData["provincePostalCodeID"];

    $provinceCodeRules =
      isset($postData["provinceCode"]) && !is_null($postData["provinceCode"])
        ? [
          "provinceCode" => function ($attribute, $value, $fail) use ($provincePostalCodeID) {
            $this->validateUniqueFromExistingRows($value,$fail,$provincePostalCodeID);
          },
        ]
        : [];

  
    $oldData = Models\ProvincePostalCode::find(
      $postData["provincePostalCodeID"]
    )->toArray();

    $config = [
      "event" => "Update",
      "referenceID" => $postData["provincePostalCodeID"],
      "userAgent" => $request->server('HTTP_USER_AGENT'),
      "ipAddress" => $request->ip(),
      "timestamp" => timestamp(),
    ];

    Facades\DB::transaction(function () use ($oldData, $postData, $config) {
     

      $postalCode = Models\ProvincePostalCode::find(
        $postData["provincePostalCodeID"]
      );

      $postalCode->province = $postData["province"];

      $postalCode->provinceCode = $postData["provinceCode"] ?? null;

      $postalCode->min = $postData["min"];

      $postalCode->max = $postData["max"];

      $postalCode->save();

      # Audit Log

      Helpers\AuditLog::create(
        "province-postal-code",
        $config,
        $oldData,
        $postData
      );
    });

    return response()->json([
      "message" => "Postal code record has been successfully updated!",
    ]);
  }


  public function upload(Request $request)
  {
    $param = [self::MODULE_PATH, Facades\Config::get("constants.feature.add")];

    if (!Facades\Gate::authorize("feature-access", $param)) {
      return;
    }

    

    $path = $request->file("provincePostalCodeFile")->getRealPath();

    $extension = $request
      ->file("provincePostalCodeFile")
      ->getClientOriginalExtension();

    $readerType =
      $extension === "xlsx"
        ? \Maatwebsite\Excel\Excel::XLSX
        : \Maatwebsite\Excel\Excel::XLS;

    $import = new ProvinceManagementImport();

    Excel::import($import, $path, null, $readerType);

    $rows = $import->getRows();

   
    $invalid = [];
    $valid = $rows;

    if ($validator->fails()) {
      $errors = $validator->errors()->toArray();

      foreach ($errors as $key => $error) {
        $keyExplode = explode(".", $key);

        $index = $keyExplode[0];

        if (in_array($index, $invalid)) {
          continue;
        }

        $invalid[] = $keyExplode[0]; // push index
      }
    }

    $valid = array_filter(
      $rows,
      function ($index) use ($invalid) {
        return !in_array($index, $invalid);
      },
      ARRAY_FILTER_USE_KEY
    );

    if (empty($valid)) {
      return response()->json([
        "success" => false,
        "message" => $this->generateErrorMessage($errors),
      ]);
    }

    $validRecords = array_unique($valid, SORT_REGULAR);

    $config = [
      "event" => "Create",
      "userAgent" => $request->server('HTTP_USER_AGENT'),
      "ipAddress" => $request->ip(),
      "timestamp" => timestamp(),
    ];

    Facades\DB::transaction(function () use ($validRecords, $config) {
      foreach ($validRecords as $validRecord) {
        $postalCode = new Models\ProvincePostalCode();

        $postalCode->province = $validRecord["province"];

        $postalCode->provinceCode = $validRecord["provinceCode"] ?? null;

        $postalCode->min = $validRecord["min"];

        $postalCode->max = $validRecord["max"];

        $postalCode->save();

        # Audit Log

        $config["referenceID"] = $postalCode->provincePostalCodeID;

        Helpers\AuditLog::create(
          "province-postal-code",
          $config,
          [],
          $validRecord
        );
      }
    });

    return empty($invalid)
      ? response()->json([
        "success" => true,
        "message" => "New postal code(s) successfully added."
      ])
      : response()->json([
        "success" => false,
        "message" => $this->generateErrorMessage($errors, $validRecords)
      ]);
  }
 

  public function validateRangeFromExistingRows(string $attr,$min,$max,$fail,?int $provincePostalCodeID = null) {
    $postalCodes = Models\ProvincePostalCode::whereNull(
      "timestampDeleted"
    )->get();

    if (!is_null($provincePostalCodeID)) {
      $postalCodes = $postalCodes->except($provincePostalCodeID);
    }

    $msg = "The {$attr} field must have no overlap range with other postal codes.";

    foreach ($postalCodes as $postalCode) {
      if ($postalCode->min <= $min && $postalCode->max >= $min) {
        $fail($msg);
        continue;
      }

      if ($postalCode->min <= $max && $postalCode->max >= $max) {
        $fail($msg);
        continue;
      }

      if ($min <= $postalCode->min && $max >= $postalCode->min) {
        $fail($msg);
        continue;
      }

      if ($min <= $postalCode->max && $max >= $postalCode->max) {
        $fail($msg);
        continue;
      }
    }
  }


  public function validateRangeFromFileRows(string $attr,$index,array $rows,$fail) {
    $msg = "The {$attr} field must have no overlap range with other postal codes.";

    $min = $rows[$index]["min"];
    $max = $rows[$index]["max"];

    foreach ($rows as $idx => $row) {
      if ($idx == $index) {
        continue;
      }

      $fileRowMin = $row["min"];
      $fileRowMax = $row["max"];

      if ($fileRowMin <= $min && $fileRowMax >= $min) {
        $fail($msg);
        continue;
      }

      if ($fileRowMin <= $max && $fileRowMax >= $max) {
        $fail($msg);
        continue;
      }

      if ($min <= $fileRowMin && $max >= $fileRowMin) {
        $fail($msg);
        continue;
      }

      if ($min <= $fileRowMax && $max >= $fileRowMax) {
        $fail($msg);
        continue;
      }
    }
  }


  public function validateUniqueFromExistingRows($provinceCode,$fail,?int $provincePostalCodeID = null) {
    $rows = Models\ProvincePostalCode::whereNull("timestampDeleted")->get([
      "provincePostalCodeID",
      "provinceCode",
    ]);

    if (!is_null($provincePostalCodeID)) {
      $rows = $rows->except($provincePostalCodeID);
    }

    foreach ($rows as $row) {
      if ($row->provinceCode == $provinceCode) {
        $fail("The Province Code field has already been taken.");
      }
    }
  }


  public function validateUniqueFromFileRows($index, array $rows, $fail)
  {
    $provinceCode = $rows[$index]["provinceCode"] ?? null;

    if (is_null($provinceCode)) {
      return;
    }

    foreach ($rows as $idx => $row) {
      if ($idx == $index) {
        continue;
      }

      $fileRowProvinceCode = $row["provinceCode"] ?? null;

      if (is_null($fileRowProvinceCode)) {
        continue;
      }

      if ($provinceCode == $fileRowProvinceCode) {
        $fail("The Province Code field has already been taken.");
      }
    }
  }

  private function generateErrorMessage($errors, $validRows = []): array
  {
    $invalid = [];
    $invalidRows = [];

    if (!empty($validRows)) {
      foreach ($validRows as $key => $validRow) {
        $rowNo = $key + 1;

        $valid[] = "Row {$rowNo}";
      }
    }

    foreach ($errors as $key => $error) {
      $keyExplode = explode(".", $key);

      $index = $keyExplode[0];

      $rowNo = (int) $index + 1;

      $invalidRows["Row {$rowNo}"][] = current($error);
    }

    ksort($invalidRows);

    foreach ($invalidRows as $key => $row) {
      $message = preg_replace("/[0-9]+\.province/", "Province", $row);

      $invalid[] = ["label" => $key, "errors" => $message];
    }

    return empty($valid)
      ? ["invalid" => $invalid]
      : ["valid" => $valid, "invalid" => $invalid];
  }

  public function delete(Request $request)
  {
    $param = [
      self::MODULE_PATH,
      Facades\Config::get("constants.feature.delete")
    ];

    if (!Facades\Gate::authorize("feature-access", $param)) {
      return;
    }

   

    $provincePostalCodeID = null;

    if ($request->filled("id")) {
      $provincePostalCodeID = Hashids::decode($request->id)[0];

   

    if (is_null($provincePostalCodeID)) {
      $deleteRecords = Models\ProvincePostalCode::whereNull(
        "timestampDeleted"
      )->get();

      $provincePostalCodeIDs = $deleteRecords->pluck("provincePostalCodeID");
    } else {
      $deleteRecords = Models\ProvincePostalCode::where(
        "provincePostalCodeID",
        $provincePostalCodeID
      )->get();

      $provincePostalCodeIDs = [$provincePostalCodeID];
    }

    $oldRecords = $deleteRecords->toArray();

    $config = [
      "event" => "Delete",
      "userAgent" => $request->server('HTTP_USER_AGENT'),
      "ipAddress" => $request->ip(),
      "timestamp" => timestamp(),
    ];

    Facades\DB::transaction(function () use (
      $provincePostalCodeIDs,
      $oldRecords,
      $config
    ) {
      Models\ProvincePostalCode::whereIn(
        "provincePostalCodeID",
        $provincePostalCodeIDs
      )->update(["timestampDeleted" => $config["timestamp"]]);

 

      foreach ($oldRecords as $oldRecord) {
        $config["referenceID"] = $oldRecord["provincePostalCodeID"];
        
        Helpers\AuditLog::create("province-postal-code",$config,$oldRecord,[]);
      }
    });

    return response()->json([
      "message" => "Postal code record(s) has been successfully deleted!"
    ]);
  }
}
}
