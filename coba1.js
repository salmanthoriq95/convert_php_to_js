class ProvinceManagementController extends Controller {
  static MODULE_PATH = "apu-ppt/province-management";

  getList(request: DataGridRequest) {
    var result = Models.ProvincePostalCode.whereNull("timestampDeleted").get();
    result = result.encode(["provincePostalCodeID"]);

    if (request.filled("search")) {
      var search = request.search;
      result = result.filter(row => {
        return collect(row).contains(value => {
          return stripos(value, search) !== false;
        });
      });
    }

    var sortField = request.sortField ?? "min";
    var sortOrder = !request.filled("sortOrder") || request.sortOrder === "ASC" ? "sortBy" : "sortByDesc";
    result = result[sortOrder](sortField, SORT_FLAG_CASE | SORT_NATURAL);
    var total = result.count();
    var rows = result.forPage(request.paginationPage, request.paginationRows).values();
    return response().json({
      rows: rows,
      total: total
    });
  }

  getItem(request: Request) {
    var provincePostalCodeID = Hashids.decode(request.id)[0];
    var row = Models.ProvincePostalCode.find(provincePostalCodeID);
    row = collect(row).encode(["provincePostalCodeID"]);

    if (row.isEmpty()) {
      throw new ModelNotFoundException().setModel("Province Postal Code");
    }

    return response().json(row);
  }

  create(request: Request) {
    var param = [this.constructor.MODULE_PATH, Facades.Config.get("constants.feature.add")];

    if (!Facades.Gate.authorize("feature-access", param)) {
      return;
    }

    var postData = collect(request.input()).toArray();
    var provinceCodeRules = undefined !== postData.provinceCode && !is_null(postData.provinceCode) ? {
      provinceCode: (attribute, value, fail) => {
        this.validateUniqueFromExistingRows(value, fail);
      }
    } : Array();
    var config = {
      event: "Create",
      userAgent: request.server("HTTP_USER_AGENT"),
      ipAddress: request.ip(),
      timestamp: timestamp()
    };
    Facades.DB.transaction(() => {
      var postalCode = new Models.ProvincePostalCode();
      postalCode.province = postData.province;
      postalCode.provinceCode = postData.provinceCode ?? undefined;
      postalCode.min = postData.min;
      postalCode.max = postData.max;
      postalCode.save();
      config.referenceID = postalCode.provincePostalCodeID;
      Helpers.AuditLog.create("province-postal-code", config, Array(), postData);
    });
    return response().json({
      message: "New postal code has been successfully created!"
    });
  }

  update(request: Request) {
    var param = [this.constructor.MODULE_PATH, Facades.Config.get("constants.feature.edit")];

    if (!Facades.Gate.authorize("feature-access", param)) {
      return;
    }

    var postData = collect(request.input()).decode(["provincePostalCodeID"]).toArray();
    var provincePostalCodeID = postData.provincePostalCodeID;
    var provinceCodeRules = undefined !== postData.provinceCode && !is_null(postData.provinceCode) ? {
      provinceCode: (attribute, value, fail) => {
        this.validateUniqueFromExistingRows(value, fail, provincePostalCodeID);
      }
    } : Array();
    var oldData = Models.ProvincePostalCode.find(postData.provincePostalCodeID).toArray();
    var config = {
      event: "Update",
      referenceID: postData.provincePostalCodeID,
      userAgent: request.server("HTTP_USER_AGENT"),
      ipAddress: request.ip(),
      timestamp: timestamp()
    };
    Facades.DB.transaction(() => //Audit Log
    {
      var postalCode = Models.ProvincePostalCode.find(postData.provincePostalCodeID);
      postalCode.province = postData.province;
      postalCode.provinceCode = postData.provinceCode ?? undefined;
      postalCode.min = postData.min;
      postalCode.max = postData.max;
      postalCode.save();
      Helpers.AuditLog.create("province-postal-code", config, oldData, postData);
    });
    return response().json({
      message: "Postal code record has been successfully updated!"
    });
  }

  upload(request: Request) {
    var param = [this.constructor.MODULE_PATH, Facades.Config.get("constants.feature.add")];

    if (!Facades.Gate.authorize("feature-access", param)) {
      return;
    }

    var path = request.file("provincePostalCodeFile").getRealPath();
    var extension = request.file("provincePostalCodeFile").getClientOriginalExtension();
    var readerType = extension === "xlsx" ? global.Maatwebsite.Excel.Excel.XLSX : global.Maatwebsite.Excel.Excel.XLS;
    var import = new ProvinceManagementImport();
    Excel.import(import, path, undefined, readerType);
    var rows = import.getRows();
    var invalid = Array();
    var valid = rows;

    if (validator.fails()) {
      var errors = validator.errors().toArray();

      for (var key in errors) //push index
      {
        var error = errors[key];
        var keyExplode = key.split(".");
        var index = keyExplode[0];

        if (-1 !== invalid.indexOf(index)) {
          continue;
        }

        invalid.push(keyExplode[0]);
      }
    }

    valid = rows.filter(index => {
      return !(-1 !== invalid.indexOf(index));
    }, ARRAY_FILTER_USE_KEY);

    if (!valid) {
      return response().json({
        success: false,
        message: this.generateErrorMessage(errors)
      });
    }

    var validRecords = array_unique(valid, SORT_REGULAR);
    var config = {
      event: "Create",
      userAgent: request.server("HTTP_USER_AGENT"),
      ipAddress: request.ip(),
      timestamp: timestamp()
    };
    Facades.DB.transaction(() => {
      for (var validRecord of Object.values(validRecords)) //Audit Log
      {
        var postalCode = new Models.ProvincePostalCode();
        postalCode.province = validRecord.province;
        postalCode.provinceCode = validRecord.provinceCode ?? undefined;
        postalCode.min = validRecord.min;
        postalCode.max = validRecord.max;
        postalCode.save();
        config.referenceID = postalCode.provincePostalCodeID;
        Helpers.AuditLog.create("province-postal-code", config, Array(), validRecord);
      }
    });
    return !invalid ? response().json({
      success: true,
      message: "New postal code(s) successfully added."
    }) : response().json({
      success: false,
      message: this.generateErrorMessage(errors, validRecords)
    });
  }

  validateRangeFromExistingRows(attr: string, min, max, fail, provincePostalCodeID: ?number = undefined) {
    var postalCodes = Models.ProvincePostalCode.whereNull("timestampDeleted").get();

    if (!is_null(provincePostalCodeID)) {
      postalCodes = postalCodes.except(provincePostalCodeID);
    }

    var msg = `The ${attr} field must have no overlap range with other postal codes.`;

    for (var postalCode of Object.values(postalCodes)) {
      if (postalCode.min <= min && postalCode.max >= min) {
        fail(msg);
        continue;
      }

      if (postalCode.min <= max && postalCode.max >= max) {
        fail(msg);
        continue;
      }

      if (min <= postalCode.min && max >= postalCode.min) {
        fail(msg);
        continue;
      }

      if (min <= postalCode.max && max >= postalCode.max) {
        fail(msg);
        continue;
      }
    }
  }

  validateRangeFromFileRows(attr: string, index, rows: {} | any[], fail) {
    var msg = `The ${attr} field must have no overlap range with other postal codes.`;
    var min = rows[index].min;
    var max = rows[index].max;

    for (var idx in rows) {
      var row = rows[idx];

      if (idx == index) {
        continue;
      }

      var fileRowMin = row.min;
      var fileRowMax = row.max;

      if (fileRowMin <= min && fileRowMax >= min) {
        fail(msg);
        continue;
      }

      if (fileRowMin <= max && fileRowMax >= max) {
        fail(msg);
        continue;
      }

      if (min <= fileRowMin && max >= fileRowMin) {
        fail(msg);
        continue;
      }

      if (min <= fileRowMax && max >= fileRowMax) {
        fail(msg);
        continue;
      }
    }
  }

  validateUniqueFromExistingRows(provinceCode, fail, provincePostalCodeID: ?number = undefined) {
    var rows = Models.ProvincePostalCode.whereNull("timestampDeleted").get(["provincePostalCodeID", "provinceCode"]);

    if (!is_null(provincePostalCodeID)) {
      rows = rows.except(provincePostalCodeID);
    }

    for (var row of Object.values(rows)) {
      if (row.provinceCode == provinceCode) {
        fail("The Province Code field has already been taken.");
      }
    }
  }

  validateUniqueFromFileRows(index, rows: {} | any[], fail) {
    var provinceCode = rows[index].provinceCode ?? undefined;

    if (is_null(provinceCode)) {
      return;
    }

    for (var idx in rows) {
      var row = rows[idx];

      if (idx == index) {
        continue;
      }

      var fileRowProvinceCode = row.provinceCode ?? undefined;

      if (is_null(fileRowProvinceCode)) {
        continue;
      }

      if (provinceCode == fileRowProvinceCode) {
        fail("The Province Code field has already been taken.");
      }
    }
  }

  generateErrorMessage(errors, validRows = Array()) {
    var invalid = Array();
    var invalidRows = Array();

    if (!!validRows) {
      for (var key in validRows) {
        var validRow = validRows[key];
        var rowNo = key + 1;
        valid.push(`Row ${rowNo}`);
      }
    }

    for (var key in errors) {
      var error = errors[key];
      var keyExplode = key.split(".");
      var index = keyExplode[0];
      rowNo = +(index + 1);
      invalidRows[`Row ${rowNo}`].push(current(error));
    }

    ksort(invalidRows);

    for (var key in invalidRows) {
      var row = invalidRows[key];
      var message = row.replace(/[0-9]+\.province/g, "Province");
      invalid.push({
        label: key,
        errors: message
      });
    }

    return !valid ? {
      invalid: invalid
    } : {
      valid: valid,
      invalid: invalid
    };
  }

  delete(request: Request) {
    var param = [this.constructor.MODULE_PATH, Facades.Config.get("constants.feature.delete")];

    if (!Facades.Gate.authorize("feature-access", param)) {
      return;
    }

    var provincePostalCodeID = undefined;

    if (request.filled("id")) {
      provincePostalCodeID = Hashids.decode(request.id)[0];

      if (is_null(provincePostalCodeID)) {
        var deleteRecords = Models.ProvincePostalCode.whereNull("timestampDeleted").get();
        var provincePostalCodeIDs = deleteRecords.pluck("provincePostalCodeID");
      } else {
        deleteRecords = Models.ProvincePostalCode.where("provincePostalCodeID", provincePostalCodeID).get();
        provincePostalCodeIDs = [provincePostalCodeID];
      }

      var oldRecords = deleteRecords.toArray();
      var config = {
        event: "Delete",
        userAgent: request.server("HTTP_USER_AGENT"),
        ipAddress: request.ip(),
        timestamp: timestamp()
      };
      Facades.DB.transaction(() => {
        Models.ProvincePostalCode.whereIn("provincePostalCodeID", provincePostalCodeIDs).update({
          timestampDeleted: config.timestamp
        });

        for (var oldRecord of Object.values(oldRecords)) {
          config.referenceID = oldRecord.provincePostalCodeID;
          Helpers.AuditLog.create("province-postal-code", config, oldRecord, Array());
        }
      });
      return response().json({
        message: "Postal code record(s) has been successfully deleted!"
      });
    }
  }

};
